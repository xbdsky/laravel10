<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Context;
use App\States\StateA;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/process-order', [OrderController::class, 'processOrder']);

Route::get('/example', function () {
    $context = new Context(new StateA()); // 初始状态设置为 StateA
    $context->request(); // 开始处理，将根据当前状态执行相应逻辑
    echo "Current state: " . $context->getCurrentState(); // 输出当前状态类名
});
