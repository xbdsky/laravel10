<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/



Route::controller(RegisterController::class)->group(function(){
    Route::post('register', 'register');
    Route::post('login', 'login');
});

Route::middleware('auth:sanctum')->group( function () {
    Route::resource('products', ProductController::class);
});

Route::post("/test/send", [\App\Http\Controllers\API\TestController::class, 'send']);
Route::post("/test/upload", [\App\Http\Controllers\API\TestController::class, 'upload']);
Route::get("/test/getFile", [\App\Http\Controllers\API\TestController::class, 'getFile']);
Route::post("/test/t3", [\App\Http\Controllers\API\TestController::class, 't3']);
