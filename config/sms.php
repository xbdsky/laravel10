<?php

return [
    'accessKeyId' => env('ALIYUN_SMS_ACCESS_KEY_ID', 'your-access-key-id'),
    'accessKeySecret' => env('ALIYUN_SMS_ACCESS_KEY_SECRET', 'your-access-key-secret'),
    'signName' => env('ALIYUN_SMS_SIGN_NAME', 'your-sign-name'), // 短信签名
    'templateCode' => env('ALIYUN_SMS_TEMPLATE_CODE', 'your-template-code'), // 短信模板CODE
];