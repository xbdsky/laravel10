<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


function deal(){
    $datas = [
        [
            'component' => 'system',
            'redict' => '/system',
            'path' => '/system',
            'hidden' => 1,
            'name' => '系统管理',
            'children' => [
                [
                    'component' => 'user',
                    'redict' => '/user',
                    'path' => '/user',
                    'hidden' => 1,
                    'name' => '用户管理',
                    'children' => [
                        [
                            'component' => 'user_list',
                            'redict' => '/user_list',
                            'path' => '/user_list',
                            'hidden' => 1,
                            'name' => '用户列表',
                        ],
                        [
                            'component' => 'role',
                            'redict' => '/user_role',
                            'path' => '/user_role',
                            'hidden' => 1,
                            'name' => '角色列表',
                        ],
                        [
                            'component' => 'perm',
                            'redict' => '/perm',
                            'path' => '/perm',
                            'hidden' => 1,
                            'name' => '权限列表',
                        ],
                    ]
                ],
                [
                    'component' => 'data',
                    'redict' => '/data',
                    'path' => '/data',
                    'hidden' => 1,
                    'name' => '数据管理',
                    'children' => [
                        [
                            'component' => 'report',
                            'redict' => '/report',
                            'path' => '/report',
                            'hidden' => 1,
                            'name' => '分日报表',
                        ],
                        [
                            'component' => 'hour_report',
                            'redict' => '/hour_report',
                            'path' => '/hour_report',
                            'hidden' => 1,
                            'name' => '分时报表',
                        ],
                        [
                            'component' => 'month_report',
                            'redict' => '/month_report',
                            'path' => '/month_report',
                            'hidden' => 1,
                            'name' => '分月报表',
                        ],
                    ]
                ],
                [
                    'component' => 'resource',
                    'redict' => '/resource',
                    'path' => '/resource',
                    'hidden' => 1,
                    'name' => '资源管理',
                    'children' => [
                        [
                            'component' => 'res_list',
                            'redict' => '/res_list',
                            'path' => '/res_list',
                            'hidden' => 1,
                            'name' => '资源列表'
                        ]
                    ]
                ]
            ]
        ]
    ];

    $level_1_tree = [];
    foreach($datas as $data_level_1){
        $data_level_1_arr['component'] = $data_level_1['component'];
        $data_level_1_arr['redict'] = $data_level_1['redict'];
        $data_level_1_arr['path'] = $data_level_1['path'];
        $data_level_1_arr['meta'] = [
             'title' => $data_level_1['name'],
             'hidden' => $data_level_1['hidden']
        ];
        $level_2_tree = [];
        if(isset($data_level_1['children']) && !empty($data_level_1['children'])) {
            foreach ($data_level_1['children'] as $data_level_2) {
                $data_level_2_arr['component'] = $data_level_2['component'];
                $data_level_2_arr['redict'] = $data_level_2['redict'];
                $data_level_2_arr['path'] = $data_level_2['path'];
                $data_level_2_arr['meta'] = [
                 'title' => $data_level_2['name'],
                 'hidden' => $data_level_2['hidden']
                ];
                $level_3_tree = [];
                if (isset($data_level_2['children']) && !empty($data_level_2['children'])) {
                
                   foreach ($data_level_2['children'] as $data_level_3) {
                        $data_level_3_arr['component'] = $data_level_3['component'];
                        $data_level_3_arr['redict'] = $data_level_3['redict'];
                        $data_level_3_arr['path'] = $data_level_3['path'];
                        $data_level_3_arr['meta'] = [
                           'title' => $data_level_3['name'],
                           'hidden' => $data_level_3['hidden']
                        ];
                        array_push($level_3_tree, $data_level_3_arr);
                        unset($data_level_3_arr);
                   }
                   $data_level_2_arr['children'] = $level_3_tree;
                }
                array_push($level_2_tree, $data_level_2_arr);
                unset($data_level_2_arr);
            }
            $data_level_1_arr['children'] = $level_2_tree;
        }
        
        array_push($level_1_tree, $data_level_1_arr);
        unset($data_level_1_arr);
    }
    return ($level_1_tree);
}

deal();
