<?php
header("Content-type: text/html; charset=utf-8");
/**
 * @author dawawa
 * @copyright 2020
 */

$partnerID = "Y7hHmkqf";//此处替换为您在丰桥平台获取的顾客编码
$checkword = "ehpA2SxjBT0M0QY74VktZbPSx41xO9gt";//此处替换为您在丰桥平台获取的校验码


//----以下为请求服务接口和消息内容---
//$serviceCode = "EXP_RECE_CREATE_ORDER";
//$file = './callExpressRequest/01.order.json';//下订单

//$serviceCode = "EXP_RECE_SEARCH_ORDER_RESP";
//$file = './callExpressRequest/02.order.query.json';//订单结果查询

//$serviceCode = "EXP_RECE_UPDATE_ORDER";
//$file = './callExpressRequest/03.order.confirm.json';//订单确认取消

//$serviceCode = "EXP_RECE_FILTER_ORDER_BSP";
//$file = './callExpressRequest/04.order.filter.json';//订单筛选	

$serviceCode = "EXP_RECE_SEARCH_ROUTES";
$file = './callExpressRequest/05.route_query_by_MailNo.json';//路由查询-通过运单号
//$file = './callExpressRequest/05.route_query_by_OrderNo.json';//路由查询-通过订单号 


//$serviceCode = "EXP_RECE_GET_SUB_MAILNO";
//$file = './callExpressRequest/07.sub.mailno.json';//子单号申请


// $serviceCode = "EXP_RECE_QUERY_SFWAYBILL";
// $file = './callExpressRequest/09.waybills_fee.json';//清单运费查询

$msgData = file_get_contents($file);//读取文件内容
var_dump( $msgData  );

//获取UUID
function create_uuid() {
    $chars = md5(uniqid(mt_rand(), true));
    $uuid = substr ( $chars, 0, 8 ) . '-'
        . substr ( $chars, 8, 4 ) . '-'
        . substr ( $chars, 12, 4 ) . '-'
        . substr ( $chars, 16, 4 ) . '-'
        . substr ( $chars, 20, 12 );
    return $uuid ;
}
$requestID = create_uuid();

//获取时间戳
$timestamp = time();

//通过MD5和BASE64生成数字签名
$msgDigest = base64_encode(md5((urlencode($msgData .$timestamp. $checkword)), TRUE));

//POST
function send_post($url, $post_data) {
     
    $postdata = http_build_query($post_data);
    $options = array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type:application/x-www-form-urlencoded;charset=utf-8',
            'content' => $postdata,
            'timeout' => 15 * 60 // 超时时间（单位:s）
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    return $result;
}

//发送参数
$post_data = array(
    'partnerID' => $partnerID,
    'requestID' => $requestID,
    'serviceCode' => $serviceCode,
    'timestamp' => $timestamp,
    'msgDigest' => $msgDigest,
    'msgData' => $msgData
);

//沙箱环境的地址
$CALL_URL_BOX = "http://sfapi-sbox.sf-express.com/std/service";
//生产环境的地址
$CALL_URL_PROD = "https://sfapi.sf-express.com/std/service";

$resultCont = send_post($CALL_URL_BOX, $post_data); //沙盒环境

print_r(json_decode($resultCont)); //提示重复下单请修改json文件内对应orderid参数
