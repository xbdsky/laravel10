<?php
header("Content-type: text/html; charset=utf-8");
/**
 * @author dawawa
 * @copyright 2020
 */

//$partnerID = "Y7hHmkqf";//此处替换为您在丰桥平台获取的顾客编码
//$checkword = "ehpA2SxjBT0M0QY74VktZbPSx41xO9gt";//此处替换为您在丰桥平台获取的校验码

$partnerID = "TZGJK1GSO64M"; //此处替换为您在丰桥平台获取的顾客编码
//$checkword = "Eys18BspjFDrQg2FmAfkqvCcqw7MaMzV";//此处替换为您在丰桥平台获取的沙盒校验码
$checkword = "S8rogwyUnn9rjcv296C6z6WikxXXhoVb";//此处替换为您在丰桥平台获取的生产校验码

//----以下为请求服务接口和消息内容---
//$serviceCode = "EXP_RECE_CREATE_ORDER";
//$file = './callExpressRequest/01.order.json';//下订单

//$serviceCode = "EXP_RECE_SEARCH_ORDER_RESP";
//$file = './callExpressRequest/02.order.query.json';//订单结果查询

//$serviceCode = "EXP_RECE_UPDATE_ORDER";
//$file = './callExpressRequest/03.order.confirm.json';//订单确认取消

//$serviceCode = "EXP_RECE_FILTER_ORDER_BSP";
//$file = './callExpressRequest/04.order.filter.json';//订单筛选	

$serviceCode = "EXP_RECE_SEARCH_ROUTES";
$file = './callExpressRequest/05.route_query_by_MailNo.json';//路由查询-通过运单号
//$file = './callExpressRequest/05.route_query_by_OrderNo.json';//路由查询-通过订单号 

//$serviceCode = "EXP_RECE_GET_SUB_MAILNO";
//$file = './callExpressRequest/07.sub.mailno.json';//子单号申请

// $serviceCode = "EXP_RECE_QUERY_SFWAYBILL";
// $file = './callExpressRequest/09.waybills_fee.json';//清单运费查询

//$serviceCode = "EXP_RECE_REGISTER_ROUTE";
//$file = './callExpressRequest/12.register_route.json';//路由注册

// $serviceCode = "EXP_RECE_CREATE_REVERSE_ORDER";
// $file = './callExpressRequest/13.reverse_order.json';//退货下单

// $serviceCode = "EXP_RECE_CANCEL_REVERSE_ORDER";
// $file = './callExpressRequest/14.cancel_reverse_order.json';//退货消单

// $serviceCode = "EXP_RECE_DELIVERY_NOTICE";
// $file = './callExpressRequest/15.delivery_notice.json';//派件通知

// $serviceCode = "EXP_RECE_REGISTER_WAYBILL_PICTURE";
// $file = './callExpressRequest/16.register_waybill_picture.json';//图片注册及推送
	
// $serviceCode = "EXP_RECE_WANTED_INTERCEPT";
// $file = './callExpressRequest/18.wanted_intercept.json';//截单转寄
 
// $serviceCode = "EXP_RECE_QUERY_DELIVERTM";
// $file = './callExpressRequest/19.query_delivertm.json';//派件通知

// $serviceCode = "COM_RECE_CLOUD_PRINT_WAYBILLS";
// $file = './callExpressRequest/20.cloud_print_waybills.json';//云打印面单打印

// $serviceCode = "EXP_RECE_UPLOAD_ROUTE";
// $file = './callExpressRequest/21.upload_route.json';//路由上传

// $serviceCode = "EXP_RECE_SEARCH_PROMITM";
// $file = './callExpressRequest/22.search_promitm.json';//预计派送时间

// $serviceCode = "EXP_EXCE_CHECK_PICKUP_TIME";
// $file = './callExpressRequest/23.check_pickup_time.json';//揽件服务时间

// $serviceCode = "EXP_RECE_VALIDATE_WAYBILLNO";
// $file = './callExpressRequest/24.validate_waybillno.json';//运单号合法性校验

//$msgData = file_get_contents($file);//读取文件内容 SF1452081771692

//print_r($msgData);


$msgData = '{"language":0,"trackingType":1,"trackingNumber":["SF1456886859669"],"methodType":1,"checkPhoneNo":"9356"}';

// $msgData = '{
// 	"language": "0",
// 	"trackingType": "1",
// 	"trackingNumber": ["SF7444407228423"],
// 	"methodType": "1"
// }';

// $msgData = '{
// 	"language": "0",
// 	"trackingType": "2",
// 	"trackingNumber": ["QIAO-20200605-003"],
// 	"methodType": "1"
// }';
// $msgData = '{
// 	"language": "0",
// 	"trackingType": "2",
// 	"trackingNumber": ["QIAO-20200605-003"],
// 	"methodType": "1"
// }';

// $serviceCode = "RoutePushService";

// $msgData = '{
//     "Body": {
//         "WaybillRoute": [{
//             "mailno": "SF7444400031887",
//             "acceptAddress": "test",
//             "reasonName": "",
//             "orderid": "202003225d33322239ddW1df5t3",
//             "acceptTime": "2020-05-11 16:56:54",
//             "remark": "test",
//             "opCode": "50",
//             "id": "158918741444476",
//             "reasonCode": ""
//         },
//         {
//             "mailno": "SF7444400031887",
//             "acceptAddress": "test",
//             "reasonName": "",
//             "orderid": "202003225d33322239ddW1df5t3",
//             "acceptTime": "2020-05-11 16:56:54",
//             "remark": "test",
//             "opCode": "80",
//             "id": "158918741457126",
//             "reasonCode": ""
//         }]
//     }
// }';

// $msgData = '{
//     "attributeNo":"SF7444400031887",
//     "type":"1",
//     "checkPhoneNo":"",
//     "orderId":"",
//     "clientIp":"",
//     "language":"zh-CN",
//     "country":"CN"
// }';

//获取UUID
function create_uuid() {
    $chars = md5(uniqid(mt_rand(), true));
    $uuid = substr ( $chars, 0, 8 ) . '-'
        . substr ( $chars, 8, 4 ) . '-'
        . substr ( $chars, 12, 4 ) . '-'
        . substr ( $chars, 16, 4 ) . '-'
        . substr ( $chars, 20, 12 );
    return $uuid ;
}
$requestID = create_uuid();

//获取时间戳
$timestamp = time();

//通过MD5和BASE64生成数字签名
$msgDigest = base64_encode(md5((urlencode($msgData .$timestamp. $checkword)), TRUE));

//POST
function send_post($url, $post_data) {
    $postdata = http_build_query($post_data);
    $options = array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type:application/x-www-form-urlencoded;charset=utf-8',
            'content' => $postdata,
            'timeout' => 15 * 60 // 超时时间（单位:s）
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    return $result;
}

//发送参数
$post_data = array(
    'partnerID' => $partnerID,
    'requestID' => $requestID,
    'serviceCode' => $serviceCode,
    'timestamp' => $timestamp,
    'msgDigest' => $msgDigest,
    'msgData' => $msgData
);

// $post_data = array (
//     'partnerID' => 'Y7hHmkqf',
//     'requestID' => '5da96eb7-b877-a92c-513c-9a86372f287b',
//     'serviceCode' => 'EXP_RECE_SEARCH_ROUTES',
//     'timestamp' => 1715413837,
//     'msgDigest' => 'BAb5c0Y/QmdT7FznLNGgIw==',
//     'msgData' => '{"language":0,"trackingType":1,"trackingNumber":["SF7444407228423"],"methodType":1}',
// );

//沙箱环境的地址
$CALL_URL_BOX = "https://sfapi-sbox.sf-express.com/std/service";

//生产环境的地址
$CALL_URL_PROD = "https://bspgw.sf-express.com/std/service";

//$resultCont = send_post($CALL_URL_BOX, $post_data); //沙盒环境
$resultCont = send_post($CALL_URL_PROD, $post_data); //生产环境

var_dump( $resultCont );
exit;

//print_r(json_decode($resultCont)); //提示重复下单请修改json文件内对应orderid参数
$data = json_decode($resultCont);

if (!empty($data->apiResultData)) {
    $arr = json_decode($data->apiResultData,true);
    $arrData = $arr['msgData']['routeResps'][0]['routes'] ?? [];
    $listArr = array_reverse($arrData);
    foreach ($listArr as $v) {
        $resData[] = [
            "time" => $v["acceptTime"],
            "context" => $v["remark"],
            "ftime" => $v["acceptTime"],
            "areaCode" => "",//本数据元对应的行政区域编码，resultv2=1或者resultv2=4才会展示
            "areaName" => "",//本数据元对应的行政区域名称，resultv2=1或者resultv2=4才会展示
            "status" => $v["opCode"],//本数据元对应的物流状态名称或者高级物流状态名称，resultv2=1或者resultv2=4才会展示
            "location" => "", //本数据元对应的快件当前地点，resultv2=4才会展示
            "areaCenter" => "", //本数据元对应的行政区域经纬度，resultv2=4才会展示
            "areaPinYin" => "",//本数据元对应的行政区域拼音，resultv2=4才会展示
            "statusCode" => $v["opCode"]//本数据元对应的高级物流状态值，resultv2=4才会展示
        ];
    }

    $arr['express'] = $resData;
}

print_r($arr);

// 5.1 （API）平台结果代码列表
// 标识	说明
// A1000	统一接入平台校验成功，调用后端服务成功；
// 注意：不代表后端业务处理成功，实际业务处理结果，
// 需要查看响应属性apiResultData中的详细结果
// A1001	必传参数不可为空
// A1002	请求时效已过期
// A1003	IP无效
// A1004	无对应服务权限
// A1005	流量受控
// A1006	数字签名无效
// A1007	重复请求
// A1008	数据解密失败
// A1009	目标服务异常或不可达
// A1099	系统异常

// 业务异常代码
// #	errorCode	描述
// 1	S0000	成功
// 2	S0001	非法的JSON格式
// 3	S0002	必填参数%s为空
// 4	S0003	系统发生数据错误或运行时异常
// 5	S0004	参数%s超过最大长度%d
// 6	S0005	参数超过最大值
// 7	S0006	参数%s不能小于%d
// 8	S0007	参数%s数据类型错误


// 已揽收	50	顺丰已收件
// 已揽收	51	一票多件的子件
// 已揽收	54	上门收件
// 已揽收	127	寄件客户将快件放至丰巢
// 已揽收	188	KC揽收
// 已揽收	214	星管家收件
// 已揽收	43	收件入仓
// 已揽收	651	SLC已揽件
// 已揽收	655	合作点收件
// 已揽收	656	合作点交接给顺丰
// 已揽收	700	拍照收件
// 已揽收	701	一票多件拍照收件
// 已揽收	84	重量复核
// 已揽收	850	集收
// 已揽收	851	集收
// 已揽收	950	快速收件
// 取消寄件	131	快递员从丰巢收件失败
// 取消寄件	215	星管家退件给客户
// 取消寄件	660	合作点退件给客户
// 运输中	31	快件到达 【XXX集散中心】
// 运输中	302	车辆发车
// 运输中	304	离开经停点
// 运输中	36	封车操作
// 运输中	10	办事处发车/中转发车/海关发车/机场发货
// 运输中	105	航空起飞
// 运输中	106	航空落地
// 运输中	11	办事处到车/中转到车/海关到车/机场提货
// 运输中	136	落地配装车
// 运输中	137	落地配卸车
// 运输中	14	货件已放行
// 运输中	147	整车在途
// 运输中	186	KC装车
// 运输中	187	KC卸车
// 运输中	189	KC快件交接
// 运输中	190	无人车发车
// 运输中	405	船舶离港
// 运输中	406	船舶到港
// 运输中	407	接驳点收件出仓
// 运输中	41	交收件联(二程接驳收件)
// 运输中	46	二程接驳收件
// 运输中	570	铁路发车
// 运输中	571	铁路到车
// 运输中	614	清关时效延长
// 运输中	623	海关数据放行
// 运输中	631	新单退回
// 运输中	843	集收入库
// 运输中	847	二程接驳
// 运输中	88	外资航空起飞
// 运输中	89	外资航空落地
// 运输中	921	晨配在途
// 运输中	96	整车发货
// 运输中	98	代理路由信息
// 运输中	30	快件在【XXX营业点】已装车,准备发往 【XXX集散中心】
// 运输中	606	清关完成
// 运输中	64	晨配转出
// 运输中	65	晨配转入
// 运输中	86	晨配装车
// 运输中	87	晨配卸车
// 运输中	930	外配装车
// 运输中	931	外配卸车
// 运输中	932	外配交接
// 转寄	626	到转第三方快递
// 转寄	627	寄转第三方快递
// 转寄	648	快件已退回/转寄,新单号为: XXX
// 转寄	99	应客户要求,快件正在转寄中
// 海关通关中	604	CFS清关
// 海关通关中	605	运力抵达口岸
// 海关通关中	619	检疫查验
// 海关通关中	620	检疫待查
// 准备派送	34	滞留件出仓
// 准备派送	122	加时区域派件出仓
// 准备派送	123	快件正送往顺丰店/站
// 准备派送	125	快递员派件至丰巢
// 准备派送	126	快递员取消派件将快件取出丰巢
// 准备派送	130	快件到达顺丰店/站
// 准备派送	208	代理交接
// 准备派送	211	星管家派件交接
// 准备派送	212	星管家派送
// 准备派送	47	二程接驳派件
// 准备派送	607	代理收件
// 准备派送	630	落地配派件出仓
// 准备派送	634	港澳台二程接驳派件
// 准备派送	642	门市/顺丰站快件上架
// 准备派送	657	合作点从顺丰交接
// 准备派送	664	客户接触点交接
// 准备派送	678	快件到达驿站
// 准备派送	844	配送出库
// 准备派送	880	上门派件
// 准备派送	933	外配出仓
// 准备派送	44	我们正在为您的快件分配最合适的快递员，请您稍等
// 准备派送	204	派件责任交接
// 待派送滞留	33	派件异常原因
// 待派送滞留	15	海关查验
// 待派送滞留	16	正式报关待申报
// 待派送滞留	17	海关待查
// 待派送滞留	18	海关扣件
// 待派送滞留	611	理货异常
// 待派送滞留	612	暂存口岸待申报
// 待派送滞留	613	海关放行待补税
// 待派送滞留	621	检疫扣件
// 待派送滞留	66	中转批量滞留
// 待派送滞留	70	由于XXX原因 派件不成功
// 待派送滞留	72	标记异常
// 待派送滞留	77	中转滞留
// 待派送滞留	833	滞留件入仓
// 待派送滞留	934	外配异常
// 已派送成功	658	合作点已派件
// 已派送成功	679	驿站完成派件
// 已派送成功	870	非正常派件
// 已派送成功	935	外配签收
// 已派送成功	97	整车签收
// 已派送成功	80	已签收,感谢使用顺丰,期待再次为您服务
// 已派送成功	8000


