<?php


namespace App\Services;


class CommonService
{
    private static array $_instances = [];

    private function __construct()
    {
    }

    /**
     * @return static
     */
    public static function getInstance(): static
    {
        if (!isset(self::$_instances[$class = static::class])) {
            self::$_instances[$class] = new static();
        }

        self::$_instances[$class]->init();
        return self::$_instances[$class];
    }

    protected function init() {}
}
