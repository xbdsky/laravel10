<?php

namespace App\Services;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use Illuminate\Support\Arr;

class SmsService extends CommonService
{
    public function send($data): void
    {
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => env('ALIYUN_SMS_ACCESS_KEY_ID'),
            // 您的AccessKey Secret
            "accessKeySecret" => env('ALIYUN_SMS_ACCESS_KEY_SECRET')
        ]);
        // Endpoint 请参考 https://api.aliyun.com/product/Dysmsapi
        $config->endpoint = "dysmsapi.aliyuncs.com";

        $client = new Dysmsapi($config);

        $code = $this->randCode();
        $sendSmsRequest = new SendSmsRequest([
            "phoneNumbers" => "18815606924",
            "signName" => env('ALIYUN_SMS_SIGN_NAME'),
            "templateCode" => env('ALIYUN_SMS_TEMPLATE_CODE'),
            "templateParam" => json_encode(['code' => $code])
        ]);
        $runtime = new RuntimeOptions([]);
        $client->sendSmsWithOptions($sendSmsRequest, $runtime);
    }

    public function randCode()
    {
        $num = rand(10000,999999);
        return str_pad(rand(10000,999999),6,rand(1,9));
    }
}
