<?php

namespace App;

use App\States\AbstractOrderState;

class Order
{
    protected $state;

    public function setState(AbstractOrderState $state): void
    {
        $this->state = $state;
    }

    public function process()
    {
        return $this->state->handle();
    }
}
