<?php
namespace App;

use App\States\AbstractState;

class Context
{
    protected $state;

    public function __construct(AbstractState $state)
    {
        $this->state = $state;
    }

    public function setState(AbstractState $state)
    {
        $this->state = $state;
    }

    public function request()
    {
        $this->state->handle($this);
    }

    public function getCurrentState()
    {
        return get_class($this->state);
    }
}
