<?php
namespace App\States;

use App\Context;

class StateB extends AbstractState
{
    public function handle(Context $context)
    {
        // StateB 的逻辑处理
        echo "Handling State B";
        // 根据条件转换到下一个状态，这里假设转换回 StateA
        $context->setState(new StateA());
    }
}
