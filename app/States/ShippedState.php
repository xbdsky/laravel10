<?php

namespace App\States;

class ShippedState extends AbstractOrderState
{
    public function handle()
    {
        return "订单已发货.";
    }
}
