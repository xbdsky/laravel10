<?php

namespace App\States;

use App\Order;

abstract class AbstractOrderState
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    abstract public function handle();
}
