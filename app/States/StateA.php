<?php

namespace App\States;

use App\Context;

class StateA extends AbstractState
{
    public function handle(Context $context)
    {
        // StateA 的逻辑处理
        echo "Handling State A";
        // 根据条件转换到下一个状态
        $context->setState(new StateB());
    }
}
