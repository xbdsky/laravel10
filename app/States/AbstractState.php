<?php

namespace App\States;

use App\Context;

abstract class AbstractState
{
    public abstract function handle(Context $context);
}
