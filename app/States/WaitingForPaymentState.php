<?php

namespace App\States;

class WaitingForPaymentState extends AbstractOrderState
{
    public function handle()
    {
        return "订单正在等待付款.";
    }
}
