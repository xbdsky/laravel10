<?php

namespace App\States;

class PaidState extends AbstractOrderState
{
    public function handle()
    {
        return "订单已经支付.";
    }
}
