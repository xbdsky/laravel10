<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use AliyunCoreClient;
use AliyunCoreProfileDefaultProfile;
use AliyunCoreProfileIClientProfile;
use AliyunCoreRegionsRegionId;
use AliyunSmsOpenAPISendSmsRequest;
use AliyunSmsOpenAPISmsOpenAPI;

// 创建一个服务提供者
class SmsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('sms', function () {
            $accessKeyId = config('sms.accessKeyId');
            $accessKeySecret = config('sms.accessKeySecret');

            $iClientProfile = DefaultProfile::getProfile(
                "cn-hangzhou", // 地区ID
                $accessKeyId,
                $accessKeySecret
            );

            $client = new SmsOpenAPI($iClientProfile);

            return $client;
        });
    }
}
