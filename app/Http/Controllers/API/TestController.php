<?php

namespace App\Http\Controllers\API;

use App\Services\SmsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TestController extends BaseController
{
    public function send(Request $request): JsonResponse
    {
        $data = SmsService::getInstance()->send($request->input());
        var_dump($data);
        //return $this->sendResponse($data, 'Products retrieved successfully.');
    }

    public function upload()
    {
        Storage::disk('local')->put('example.txt', 'Contents');
    }

    public function getFile()
    {
        echo asset('storage/1.png');
    }

    public function t3(Request $request): JsonResponse
    {   
        $bloodsugar = '5A25001010218K137080000000075190103170607D0_460042371707173_865916031909030FFFFFFFFFF';
        //一台序列号为 18K137080 的G426-3血糖仪在 14 年 6月 12日 10時 12分，测量结果为 172mg/dL ，通过GSM传送的一笔数据.	
        $bloodsugar2 = '5A25001090214F000029000000172140612101207BB_460027966329481_863082039677939FFFFFFFFFF';
        
        // $t1 = substr(date('Y'),0,2) . substr($bloodsugar,29,8) . "00";
        // echo date('Y-m-d H:i:s', strtotime($t1));
        
        $t2 = substr(date('Y'),0,2) . substr($bloodsugar2,29,10) . "00";
        echo substr(date('Y'),0,2) . PHP_EOL;
        echo substr($bloodsugar2,29,10) . PHP_EOL;
        echo date('Y-m-d H:i:s', strtotime($t2));
        //[date("Y-m-d",Arr::get($validate, 'time'))." 00:00:00", date("Y-m-d",Arr::get($validate, 'time'))." 23:59:59"]

        //$date = substr(date('Y'),0,2) . substr($bloodsugar,29,8);
        //$date2 = substr(date('Y'),0,2) . substr($bloodsugar2,29,8);
        
        //echo strtotime($date);
        //var_dump([$date, $date2]);
        //var_dump($request->input());
    }
}
