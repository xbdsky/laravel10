<?php

namespace App\Http\Controllers;

use App\Order;
use App\States\PaidState;
use App\States\ShippedState;
use App\States\WaitingForPaymentState;

class OrderController extends Controller
{
    public function processOrder(): void
    {
        $order = new Order();

        // 设置订单状态为待付款
        $order->setState(new WaitingForPaymentState($order));
        echo $order->process() . "<br/>";

        // 设置订单状态为已付款
        $order->setState(new PaidState($order));
        echo $order->process() . "<br/>";

        // 设置订单状态为已发货
        $order->setState(new ShippedState($order));
        echo $order->process() . "<br/>";
    }
}
